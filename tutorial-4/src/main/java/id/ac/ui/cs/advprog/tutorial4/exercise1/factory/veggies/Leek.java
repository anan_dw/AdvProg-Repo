package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Leek implements Veggies {

    public String toString() {
        return "Leek";
    }
}
