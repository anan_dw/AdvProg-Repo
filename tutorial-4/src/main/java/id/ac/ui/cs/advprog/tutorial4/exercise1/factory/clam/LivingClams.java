package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class LivingClams implements Clams {

    public String toString() {
        return "Living Clams from Bermuda Triangle";
    }
}
