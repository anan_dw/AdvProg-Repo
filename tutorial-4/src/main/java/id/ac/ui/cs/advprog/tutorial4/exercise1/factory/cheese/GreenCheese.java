package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class GreenCheese implements Cheese {

    public String toString() {
        return "Green Cheese";
    }
}
