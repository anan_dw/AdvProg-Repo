package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class PeanutSauce implements Sauce {
    public String toString() {
        return "Peanut Sauce";
    }
}
