package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PlumTomatoSauceTest {
    private PlumTomatoSauce plumTomatoSauce;

    @Before
    public void setUp() {
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Plum Tomato Sauce", plumTomatoSauce.toString());
    }
}
