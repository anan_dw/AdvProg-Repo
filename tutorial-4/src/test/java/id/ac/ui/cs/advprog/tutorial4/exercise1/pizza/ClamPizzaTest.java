package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

public class ClamPizzaTest {
    private ClamPizza clamPizza;
    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp() {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
        clamPizza = new ClamPizza(depokPizzaIngredientFactory);
    }

    @Test
    public void testMethodPrepare() {
        clamPizza.prepare();
        assertNotNull(clamPizza.dough);
        assertNotNull(clamPizza.sauce);
        assertNotNull(clamPizza.clam);
    }
}
