package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {
    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp() {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    public void testMethodCreateDough() {
        assertNotNull(depokPizzaIngredientFactory.createDough());
    }

    @Test
    public void testMethodCreateSauce() {
        assertNotNull(depokPizzaIngredientFactory.createSauce());
    }

    @Test
    public void testMethodCreateCheese() {
        assertNotNull(depokPizzaIngredientFactory.createCheese());
    }

    @Test
    public void testMethodCreateClam() {
        assertNotNull(depokPizzaIngredientFactory.createClam());
    }

    @Test
    public void testMethodCreateVeggies() {
        assertNotNull(depokPizzaIngredientFactory.createVeggies());
    }
}