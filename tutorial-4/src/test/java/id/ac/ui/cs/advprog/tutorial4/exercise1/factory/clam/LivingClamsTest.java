package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class LivingClamsTest {
    private LivingClams livingClams;

    @Before
    public void setUp() {
        livingClams = new LivingClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Living Clams from Bermuda Triangle", livingClams.toString());
    }

}

