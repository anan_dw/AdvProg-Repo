package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FreshClamsTest {
    private FreshClams freshClams;

    @Before
    public void setUp() {
        freshClams = new FreshClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Fresh Clams from Long Island Sound", freshClams.toString());
    }

}

