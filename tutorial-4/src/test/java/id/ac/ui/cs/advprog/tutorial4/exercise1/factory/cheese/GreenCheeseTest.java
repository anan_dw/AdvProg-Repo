package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class GreenCheeseTest {
    private GreenCheese greenCheese;

    @Before
    public void setUp() {
        greenCheese = new GreenCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Green Cheese", greenCheese.toString());
    }

}

