package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PeanutSauceTest {
    private PeanutSauce peanutSauce;

    @Before
    public void setUp() {
        peanutSauce = new PeanutSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Peanut Sauce", peanutSauce.toString());
    }

}

