package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class LeekTest {
    private Leek leek;

    @Before
    public void setUp() {
        leek = new Leek();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Leek", leek.toString());
    }
}
