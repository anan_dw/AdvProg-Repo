package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ParmesanCheeseTest {
    private ParmesanCheese parmesanCheese;

    @Before
    public void setUp() {
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Shredded Parmesan", parmesanCheese.toString());
    }
}

