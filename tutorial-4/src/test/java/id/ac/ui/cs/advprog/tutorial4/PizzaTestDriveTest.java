package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class PizzaTestDriveTest {
    private PizzaStore nyStore;
    private PizzaStore dpStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
        dpStore = new DepokPizzaStore();
    }

    @Test
    public void testMethod() {
        Pizza pizza = nyStore.orderPizza("cheese");
        assertEquals(pizza.toString(), "---- New York Style Cheese Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n");

        pizza = nyStore.orderPizza("clam");
        assertEquals(pizza.toString(), "---- New York Style Clam Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n"
                + "Fresh Clams from Long Island Sound\n");

        pizza = nyStore.orderPizza("veggie");
        assertEquals(pizza.toString(), "---- New York Style Veggie Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n"
                + "Garlic, Onion, Mushrooms, Red Pepper\n");

        pizza = dpStore.orderPizza("cheese");
        assertEquals(pizza.toString(), "---- Depok Style Cheese Pizza ----\n"
                + "Black Crust Dough\n"
                + "Peanut Sauce\n"
                + "Green Cheese\n");

        pizza = dpStore.orderPizza("clam");
        assertEquals(pizza.toString(), "---- Depok Style Clam Pizza ----\n"
                + "Black Crust Dough\n"
                + "Peanut Sauce\n"
                + "Green Cheese\n"
                + "Living Clams from Bermuda Triangle\n");

        pizza = dpStore.orderPizza("veggie");
        assertEquals(pizza.toString(), "---- Depok Style Veggie Pizza ----\n"
                + "Black Crust Dough\n"
                + "Peanut Sauce\n"
                + "Green Cheese\n"
                + "Black Olives, Leek, Eggplant, Spinach\n");
    }
}
