package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BlackCrustDoughTest {
    private BlackCrustDough blackCrustDough;

    @Before
    public void setUp() {
        blackCrustDough = new BlackCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Black Crust Dough", blackCrustDough.toString());
    }
}
