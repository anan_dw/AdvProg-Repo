package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {
    private NewYorkPizzaIngredientFactory newYorkPizzaIngredientFactory;

    @Before
    public void setUp() {
        newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    public void testMethodCreateDough() {
        assertNotNull(newYorkPizzaIngredientFactory.createDough());
    }

    @Test
    public void testMethodCreateSauce() {
        assertNotNull(newYorkPizzaIngredientFactory.createSauce());
    }

    @Test
    public void testMethodCreateCheese() {
        assertNotNull(newYorkPizzaIngredientFactory.createCheese());
    }

    @Test
    public void testMethodCreateClam() {
        assertNotNull(newYorkPizzaIngredientFactory.createClam());
    }

    @Test
    public void testMethodCreateVeggies() {
        assertNotNull(newYorkPizzaIngredientFactory.createVeggies());
    }
}
