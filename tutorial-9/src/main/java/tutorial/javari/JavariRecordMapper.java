package tutorial.javari;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Body;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

public class JavariRecordMapper {
    private static final String filePath = "tutorial-9/src/main/java/tutorial/javari/animals.csv";
    private static final Path file = Paths.get("", filePath);

    public static List<Animal> getListAnimal() {
        List<Animal> animals = new ArrayList<>();

        try {
            Files.lines(file).forEach(i -> animals.add(createAnimals(i)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return animals;
    }

    public static Animal getAnimal(int id) {
        List<Animal> animals = new ArrayList<>();

        try {
            Files.lines(file)
                    .filter(i -> Integer.parseInt(i.split(",")[0]) == id)
                    .forEach(j -> animals.add(createAnimals(j)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (animals.isEmpty()) {
            return null;
        } else {
            return animals.get(0);
        }
    }

    public static Animal createAnimals(String data) {
        String[] elements = data.split(",");
        int id = Integer.parseInt(elements[0]);
        String type = elements[1].trim();
        String name = elements[2].trim();
        Gender gender = Gender.parseGender(elements[3].trim());
        double length = Double.parseDouble(elements[4]);
        double weight = Double.parseDouble(elements[5]);
        Condition condition = Condition.parseCondition(elements[6].trim());

        Animal animal = new Animal(id, type, name, gender, length, weight, condition);

        return animal;
    }

    public static Animal removeAnimal(int id)   {
        if (isExists(id)) {
            Animal animal = getAnimal(id);
            StringBuilder build = new StringBuilder();

            try {
                Files.lines(file)
                        .filter(i -> Integer.parseInt(i.split(",")[0]) != id)
                        .forEach(j -> build.append(j + "\n"));
                build.setLength(build.length() - 1);
                writeToFile(build.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return animal;
        }

        return null;
    }

    private static String createCsv(Animal animal) {
        StringBuilder build = new StringBuilder();

        build.append(animal.getId());
        build.append(animal.getType());
        build.append(animal.getName());
        build.append(animal.getGender());
        build.append(animal.getLength());
        build.append(animal.getWeight());
        build.append(animal.getCondition());

        for (int i = 0; i < build.length() - 1; i = i + 2) {
            build.insert(i + 1, ",");
        }

        return  build.toString();
    }

    public static void writeAnimalToFile(Animal animal) {
        String lines = createCsv(animal);
    }

    private static void writeToFile(String str) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(filePath));
        out.write(str);
        out.flush();
        out.close();
    }

    private static boolean isExists(int id) {
        try {
            return Files.lines(file)
                    .anyMatch(i -> Integer.parseInt(i.split(",")[0]) == id);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
