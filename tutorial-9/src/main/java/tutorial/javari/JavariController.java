package tutorial.javari;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tutorial.javari.animal.Animal;

public class JavariController {
    // TODO Implement me!
    private final AtomicLong counter = new AtomicLong();
    private List<Animal> animals;

    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    public List<Animal> getListOfAnimals() {
        return JavariRecordMapper.getListAnimal();
    }

    private List<Animal> setUpListAnimals() {
        animals = JavariRecordMapper.getListAnimal();
        counter.set(animals.get(animals.size() - 1).getId());
        return animals;
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    public Animal getAnimalById(@PathVariable("id") int id) {
        return JavariRecordMapper.getAnimal(id);
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE)
    public Animal deleteAnimal(@PathVariable("id") int id) {
        return JavariRecordMapper.removeAnimal(id);
    }

    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    public Animal addAnimal(@RequestBody String json) {
        return JavariRecordMapper.createAnimals(json);
    }
}
