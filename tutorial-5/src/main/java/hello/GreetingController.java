package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        boolean isName = false;
        if (name.length() > 0) {
            isName = true;
        }
        model.addAttribute("isName", isName);
        model.addAttribute("name", name);
        return "greeting";
    }

}
