import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    private Movie movie1;
    private Movie movie2;
    private Movie movie3;
    private Rental rent1;
    private Rental rent2;
    private Rental rent3;
    private Customer customer;

    @Before
    public void setUp() throws Exception {
        movie1 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Pacific Rim", Movie.CHILDREN);
        movie3 = new Movie("HAHAHAHA", Movie.NEW_RELEASE);
        rent1 = new Rental(movie1, 3);
        rent2 = new Rental(movie2, 2);
        rent3 = new Rental(movie3, 2);
        customer = new Customer("Alice");
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent1);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    // TODO Implement me!
    public void statementWithMultipleMovies() {
        customer.addRental(rent1);
        customer.addRental(rent2);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 11.0"));
        assertTrue(result.contains("4 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent1);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<P>Amount owed is <EM>3.5</EM><P>"));
        assertTrue(result.contains("<EM>1</EM> frequent renter points<P>"));
    }

}