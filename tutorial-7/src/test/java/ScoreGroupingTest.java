import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void groupByScoresCheck() {
        Map<Integer, List<String>> grouped = ScoreGrouping.groupByScores(scores);
        assertEquals("Alice",grouped.get(12).get(0));
    }

}